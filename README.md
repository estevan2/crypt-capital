# Crypt Capital

Cripto Capital é um pacote criado para facilitar a forma de pegar os valores
atualizado das criptomoedas e as converções entre elas e moedas tradicionais,
utilizando a api da [CoinMarketCap](https://coinmarketcap.com/api/documentation/v1/).

# Instalação

usando npm:

```
npm i crypt-capital
```

usando yarn:

```
yarn add crypt-capital
```

# Exemplo

Para usar importe a classe, crie uma nova instância e forneça sua chave api ao
instanciar a classe. Voce pode obter sua chave CoinMarketCap [aqui](https://coinmarketcap.com/api/).

```js
const APIKey = "d6e93c00-2399-4296-8ae9-3ecff4bfdf3a";
const cryto = new CryptCapital(APIKey);
```

## Atualmente estão disponiveis dois metodos ".quotes() e .conversion()":

### Metodo .quotes():

O metodo quote exige um array das siglas das cryptomoedas e retorna uma promisse:

```js
cryto.quotes(["BTC"]).then((res) => console.log(res));
```

```json
// console.log
{
  "data": {
    "BTC": {
      "id": 1,
      "name": "Bitcoin",
      "symbol": "BTC",
      "slug": "bitcoin",
      "date_added": "2013-04-28T00:00:00.000Z",
      "last_updated": "2021-08-26T17:44:11.000Z",
      "quote": {
        "USD": {
          "price": 46963.215165006586,
          "last_updated": "2021-08-26T17:44:11.000Z"
        }
      }
    }
  }
}
```

### Metodo .conversion():

Usar o metodo coversion exige três parametros: sigla da moeda, montante e um
array de moedas alvos.

```js
cryto.conversion("btc", 0.1, ["usd"]).then((res) => console.log(res));
```

```json
// console
{
  "data": {
    "id": 1,
    "symbol": "BTC",
    "name": "Bitcoin",
    "amount": 25.67,
    "last_updated": "2021-08-26T18:30:17.000Z",
    "quote": {
      "ETH": {
        "price": 386.5352847529818,
        "last_updated": "2021-08-26T18:30:16.000Z"
      }
    }
  }
}
```

## Type Guards

Exitem três Type Guards disponiveis para tratar os diferentes tipos de retorno da promisse:
`isError()`, `isQuoteResponse()` e `isConversionResponse()`:

```js
cryto.quotes(["BTC"]).then((res) => {
  if (isError(res)) {
    console.log(res.status);
  } else if (isQuoteResponse(res)) {
    console.log(res.BTC.id);
  } else {
    console.log(res);
  }
});
```

```js
cryto.conversion("btc", 0.1, ["usd"]).then((res) => {
  if (isError(res)) {
    console.log(res.status);
  } else if (isConversionResponse(res)) {
    console.log(res.data.BTC.id);
  } else {
    console.log(res);
  }
});
```

# Tecnologias

- [Axios](https://axios-http.com/)
- [Ts-Node](https://typestrong.org/ts-node/)
- [Typescript](https://www.typescriptlang.org/)

# Licença

- [MIT](https://www.mit.edu/~amini/LICENSE.md)
