export interface CryptInfo {
  price: number;
  last_updated: Date;
}

export interface CryptQuote {
  [cryptCoin: string]: CryptInfo;
}

export interface CryptCoin {
  id: number;
  name: string;
  symbol: string;
  slug: string;
  date_added: Date;
  last_updated: Date;
  quote: CryptQuote;
}

export interface CryptCoinConversion {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  last_updated: Date;
  quote: CryptQuote;
}

export interface QuotesListResponse {
  [cryptCoin: string]: CryptCoin;
}

export interface QuotesResponse {
  data: QuotesListResponse;
}

export interface ConversionResponse {
  data: CryptCoinConversion;
}

export interface ResponseError {
  timestamp: Date;
  error_code: number;
  error_message: string;
  elapsed: number;
  credit_count: number;
  notice: string | null;
}

export interface CryptResponseError {
  status: ResponseError;
}

export function isError(error: any): error is CryptResponseError {
  return (error as CryptResponseError).status?.error_code >= 400;
}

export function isQuoteResponse(res: any): res is QuotesResponse {
  const keys = Object.keys(res.data);
  return (res as QuotesResponse).data[keys[0]].id != undefined;
}

export function isConversionResponse(res: any): res is ConversionResponse {
  return (res as ConversionResponse).data.id != undefined;
}
