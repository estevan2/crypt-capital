import CryptCapital from "./model/cryptCapital";

export { CryptCapital };
export {
  CryptResponseError,
  CryptInfo,
  CryptQuote,
  CryptCoin,
  CryptCoinConversion,
  QuotesResponse,
  ConversionResponse,
  ResponseError,
  isError,
  isQuoteResponse,
  isConversionResponse,
} from "./types/cryptCoin";
