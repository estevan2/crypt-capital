import axios, { AxiosInstance, AxiosResponse } from "axios";
import { ConversionResponse, CryptResponseError, QuotesResponse, CryptQuote } from "../types/cryptCoin";

export default class CryptCapital {
  baseUrl: string = "https://pro-api.coinmarketcap.com";
  axiosInstance: AxiosInstance;

  constructor(APIKey: string) {
    this.axiosInstance = axios.create({
      baseURL: this.baseUrl,
      headers: { "X-CMC_PRO_API_KEY": APIKey },
    });
  }

  buildQuotes = (quote: CryptQuote): CryptQuote => {
    let quoteData: CryptQuote = {};
    for (let key in quote) {
      quoteData[key] = {
        price: quote[key].price,
        last_updated: new Date(quote[key].last_updated),
      };
    }
    return quoteData;
  };

  buildResponseQuotes = (response: AxiosResponse): QuotesResponse => {
    let responseData: QuotesResponse = { data: {} };

    for (let key in response.data.data) {
      let coin = response.data.data[key];
      responseData.data[key] = {
        id: coin.id,
        name: coin.name,
        symbol: coin.symbol,
        slug: coin.slug,
        date_added: new Date(coin.date_added),
        last_updated: new Date(coin.last_updated),
        quote: this.buildQuotes(coin.quote),
      };
    }

    return responseData;
  };

  buildResponseConversion = (response: AxiosResponse): ConversionResponse => {
    let responseData: ConversionResponse = {} as ConversionResponse;
    let coin = response.data.data;
    responseData.data = {
      id: coin.id,
      symbol: coin.symbol,
      name: coin.name,
      amount: coin.amount,
      last_updated: new Date(coin.last_updated),
      quote: this.buildQuotes(coin.quote),
    };

    return responseData;
  };

  async quotes(coinList: string[]) {
    let response: AxiosResponse;
    try {
      response = await this.axiosInstance.get(`/v1/cryptocurrency/quotes/latest?symbol=${coinList.join(",")}`);
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as CryptResponseError;
      }
      return undefined;
    }
    const responseData = await this.buildResponseQuotes(response);
    return responseData as QuotesResponse;
  }

  async conversion(coin: string, amount: number, convert: string[]) {
    let response: AxiosResponse;
    try {
      response = await this.axiosInstance.get(
        `/v1/tools/price-conversion?symbol=${coin}&amount=${amount}&convert=${convert.join(",")}`
      );
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as CryptResponseError;
      }
      return undefined;
    }
    const responseData = this.buildResponseConversion(response);
    return responseData as ConversionResponse;
  }
}
